FROM python:3.7-alpine

ENV TOKEN=1
ENV PREFIX="!"

WORKDIR /usr/src/app

RUN python3 -m pip install -U pipenv
COPY . /usr/src/app/
RUN pipenv install --deploy

CMD ["pipenv", "run", "start"]