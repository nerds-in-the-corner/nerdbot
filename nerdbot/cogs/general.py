import random

import discord
from discord.ext import commands


class General(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command()
    async def ping(self, ctx):
        await ctx.send(f"pong! {round(self.client.latency * 1000)}ms")

    @commands.command(aliases=["8ball"])
    async def _8ball(self, ctx, *, question):
        responses = [
        "It is certain."
        "It is decidedly so.",
        "Without a doubt.",
        "Yes - definitely.",
        "You may rely on it.",
        "As I see it, yes.",
        "Most likely.",
        "Outlook good.",
        "Yes.",
        "Signs point to yes.",
        "Reply hazy, try again.",
        "Ask again later.",
        "Better not tell you now.",
        "Cannot predict now.",
        "Concentrate and ask again.",
        "Don\'t count on it.",
        "My reply is no.",
        "My sources say no.",
        "Outlook not so good.",
        "Very doubtful."
        ]
        await ctx.send(f"Question: {question}\nAnswer: {random.choice(responses)} ")

    @commands.has_permissions(manage_messages=True)
    @commands.command()
    async def clear(self, ctx, amount=5):
        if amount <= 0:
            await ctx.send(f"{amount} is to small number to delete")
        else:
            await ctx.channel.purge(limit=amount)


def setup(client):
    client.add_cog(General(client))
