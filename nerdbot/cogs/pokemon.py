import aiohttp

import discord
from discord.ext import commands

pokeapi_url = "https://pokeapi.co/api/v2"


class Pokemon(commands.Cog):

    def __init__(self, client):
        self.client = client

    async def get_pokemon(self, id):
        async with aiohttp.ClientSession() as session:
            async with session.get(f"{pokeapi_url}/pokemon/{id}") as resp:
                return await resp.json()

    @commands.command(aliases=["poke", "p"], usage="!pokemon 1")
    async def pokemon(self, ctx, id):
        id = id.lower()
        pokemon = await self.get_pokemon(id)
        embed = discord.Embed(title=pokemon["id"], color=0x00ff19)
        embed.set_author(name=pokemon["name"])
        embed.set_thumbnail(url=pokemon["sprites"]["front_default"])
        embed.add_field(name="weight", value=pokemon["weight"], inline=True)
        embed.add_field(name="height", value=pokemon["height"], inline=True)
        await ctx.send(embed=embed)


def setup(client):
    client.add_cog(Pokemon(client))
