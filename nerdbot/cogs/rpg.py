import random

import discord
from discord.ext import commands


class Rpg(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command()
    async def dice(self, ctx, sides=6, times=1):
        sides = int(sides)
        times = int(times)

        if times < 100:
            total = [random.randint(1, sides) for i in range(times)]
            await ctx.send(f"you rolled {total}")
        else:
            _sum = 0
            total = [random.randint(1, sides) for i in range(times)]
            for num in total:
                _sum = _sum + num
            await ctx.send(f"avg: {_sum / len(total)}")


def setup(client):
    client.add_cog(Rpg(client))
