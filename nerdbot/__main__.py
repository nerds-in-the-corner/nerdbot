import os

import discord
from discord.ext import commands

from dotenv import load_dotenv

import log

# Load dotenv into the environment
load_dotenv()

# Starts up the logging instance
# Possible log levels, {
# CRITICAL: 50,
# ERROR: 40,
# WARNING: 30,
# INFO: 20,
# DEBUG: 10,
# NOTSET: 0 }
log.start_log(log_level=os.getenv("LOG_LEVEL"), filename="NerdBot.log")


# Starts up the Discord bot client
client = commands.Bot(command_prefix=os.getenv("PREFIX"),
                      case_insensitive=os.getenv("case_insensitive"))


@client.event
async def on_ready():
    print("Bot is ready")


@client.command()
async def load(ctx, extension):
    client.load_extension(f"cogs.{extension}")


@client.command()
async def unload(ctx, extension):
    client.unload_extension(f"cogs.{extension}")


@client.command()
async def reload(ctx):
    exceptions = []
    for filenames in os.listdir("./nerdbot/cogs"):
        if filenames.endswith(".py"):
            try:
                client.reload_extension(f"cogs.{filenames[:-3]}")
            except Exception as e:
                exceptions.append(e)
    if not exceptions:
        await ctx.send("All cogs have been successfully been reloaded")
    else:
        await ctx.send(f"one or more errors accured\n {exceptions}")

for filenames in os.listdir("./nerdbot/cogs"):
    if filenames.endswith(".py"):
        client.load_extension(f"cogs.{filenames[:-3]}")


if __name__ == "__main__":
    client.run(os.getenv("TOKEN"))
