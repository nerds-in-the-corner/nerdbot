import logging


def start_log(log_level="WARNING", filename="Bot.log"):
    logger = logging.getLogger('discord')
    level = getattr(logging, log_level)
    logger.setLevel(level)
    handler = logging.FileHandler(
        filename=filename, encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter(
        '%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    logger.addHandler(handler)
